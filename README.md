# Story 10
This Gitlab repository is made by YolandaWS

## Pipeline and Coverage
[![coverage report](https://gitlab.com/YolandaWS/Story10/badges/master/coverage.svg)](https://gitlab.com/YolandaWS//-/commits/master)
[![pipeline report](https://gitlab.com/YolandaWS/Story10/badges/master/pipeline.svg)](https://gitlab.com/YolandaWS//-/commits/master)

## URL
To access the website, go to [https://yolandaws-story10.herokuapp.com/](https://yolandaws-story10.herokuapp.com/)
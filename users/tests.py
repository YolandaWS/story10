from django.test import TestCase,Client
from django.urls import resolve
from .views import register,welcome
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story10UnitTest(TestCase):
    #Testing URLS
	def test_story_10_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('users/register.html')

	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('users/login.html')

	def test_logout_url_is_exist(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('users/logout.html')

	def test_welcome_url_is_exist(self):
		response = Client().get('/welcome/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('users/welcome.html')   

    #Testing Views
	def test_story_10_register_function(self):
		found = resolve('/')
		self.assertEqual(found.func,register)

	def test_welcome_function(self):
		found = resolve('/welcome/')
		self.assertEqual(found.func,welcome)
